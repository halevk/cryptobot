﻿using GekkoEx.Account.Message.Queries;
using GekkoEx.Messaging;
using GekkoEx.Messaging.Message.Commands;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;

namespace GekkoEx.Auth.Handler.Commands
{
    public class AuthorizeUserHandler : IRequestHandler<AuthorizeUser, AuthorizeUserResult>
    {
        IMessageBroker broker;
        ITokenProcessor tokenProcessor;       
        public AuthorizeUserHandler(IMessageBroker broker,ITokenProcessor tokenProcessor)
        {
            this.broker = broker;            
            this.tokenProcessor = tokenProcessor;
        }
        public async Task<AuthorizeUserResult> Handle(AuthorizeUser request, CancellationToken token = default(CancellationToken))
        {
            var result = await broker.Send(new ValidateUser { Email = request.Email, Password = request.Password });            
            if (!result.Status)
                return new AuthorizeUserResult();
            var claims = new[] {
                new Claim("Name", result.Name),
                new Claim("Email", result.Email),
                new Claim("UserName",result.UserName)
            };
            var authToken = tokenProcessor.Create(claims);
            return new AuthorizeUserResult
            {
                Status = true,
                Token = authToken
            };
        }
    }
}
