﻿using GekkoEx.Messaging;
using GekkoEx.Messaging.Message.Commands;
using System;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;

namespace GekkoEx.Auth.Handler.Commands
{
    public class ValidateTokenHandler : IRequestHandler<ValidateToken, ValidateTokenResult>
    {
        ITokenProcessor tokenProcessor;
        public ValidateTokenHandler(ITokenProcessor tokenProcessor)
        {
            this.tokenProcessor = tokenProcessor;
        }
        public async Task<ValidateTokenResult> Handle(ValidateToken request, CancellationToken token = default(CancellationToken))
        {
            var result = new ValidateTokenResult();
            ClaimsPrincipal principal;
            if (tokenProcessor.Validate(request.Token, DateTime.UtcNow, out principal))
            {
                result.Principal = principal;
                result.Status = true;
            }                
            return await Task.FromResult(result);
        }
    }
}
