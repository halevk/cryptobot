﻿namespace GekkoEx.Messaging.Message.Commands
{
    [RouteHandler(Modul = "auth", Route = "login", IsLogin = true)]
    public class AuthorizeUser : IRequest<AuthorizeUserResult>, IValidatable
    {        
        public string Email { get; set; }
        public string Password { get; set; }
        public ValidationResult Validate()
        {
            var result = new ValidationResult();
            if (string.IsNullOrWhiteSpace(Email))
                result.AddError("Email can not be empty");
            if (string.IsNullOrWhiteSpace(Email))
                result.AddError("Password can not be empty");
            return result;
        }
    }

    public class AuthorizeUserResult
    {
        public bool Status { get; set; }
        public string Token { get; set; }
    }
}
