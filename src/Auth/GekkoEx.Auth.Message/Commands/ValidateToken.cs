﻿using System.Security.Claims;

namespace GekkoEx.Messaging.Message.Commands
{
    [RouteHandler(Modul = "auth", Route = "validatetoken")]
    public class ValidateToken : IRequest<ValidateTokenResult>, IValidatable
    {
        public string Token { get; set; }
        public ValidationResult Validate()
        {
            var result = new ValidationResult();
            if (string.IsNullOrWhiteSpace(Token))
                result.AddError("Token can not be empty");
            return result;
        }
    }

    public class ValidateTokenResult
    {
        public bool Status { get; set; }
        public ClaimsPrincipal Principal { get; set; }
    }
}
