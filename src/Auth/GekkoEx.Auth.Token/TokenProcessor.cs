﻿using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace GekkoEx.Auth.Token
{
    public class TokenProcessor : ITokenProcessor
    {
        string issuer, audience, securityKey;
        DateTime expire;
        public TokenProcessor(string issuer, string audience, string securityKey, DateTime expire)
        {
            this.expire = expire;
            this.issuer = issuer;
            this.audience = audience;
            this.securityKey = securityKey;
        }
        public string Create(Claim[] claims)
        {
            var secKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(securityKey));
            var token = new JwtSecurityToken(
               issuer: issuer,
               audience: audience,
               claims: claims,
               expires: expire,
               signingCredentials: new SigningCredentials(secKey, SecurityAlgorithms.HmacSha256)
           );
            return new JwtSecurityTokenHandler().WriteToken(token);
        }

        public bool Validate(string authToken, DateTime valid, out ClaimsPrincipal principal)
        {
            SecurityToken token;
            var validationParams = new TokenValidationParameters
            {
                ValidateIssuer = true,
                ValidIssuer = issuer,
                ValidateAudience = true,
                ValidAudience = audience,
                IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(securityKey))
            };
            principal = new JwtSecurityTokenHandler().ValidateToken(authToken, validationParams, out token);
            return token.ValidTo > valid;
        }
    }
}
