﻿using GekkoEx.Auth.Token;
using GekkoEx.Config;
using GekkoEx.DI;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace GekkoEx.Auth.DI
{
    public class ServiceRegisterer : IServiceRegisterer
    {
        public void Register(IServiceCollection collection)
        {
            collection.AddSingleton<ITokenProcessor>(p => {
                var config = p.GetService<IConfig>();
                var issuer = config.Get<string>("token:issuer");
                var audience = config.Get<string>("token:audience");
                var expire = DateTime.UtcNow.AddMinutes(config.Get<int>("token:expireInMinute"));
                var secretKey = config.Get<string>("token:securityKey");
                return new TokenProcessor(issuer, audience, secretKey, expire);
            });
        }
    }
}
