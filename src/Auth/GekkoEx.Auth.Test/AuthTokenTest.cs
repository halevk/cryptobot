using GekkoEx.Auth;
using GekkoEx.Auth.Token;
using NUnit.Framework;
using System;
using System.Security.Claims;

namespace Tests
{
    public class AuthTokenTest
    {
        ITokenProcessor tokenProcessor;
        [SetUp]
        public void Setup()
        {
            tokenProcessor = new TokenProcessor("issuer1", "audience1", "!ad^+%&/HYUJKN42574hfsdfsdfs%&", DateTime.UtcNow.AddMinutes(100));
        }

        [Test]
        public void CreateAndValidateToken()
        {
            var claims = new System.Security.Claims.Claim[] {
                new System.Security.Claims.Claim("Name","test"),
                new System.Security.Claims.Claim("Email","abc@gmail.com"),
                new System.Security.Claims.Claim("Role","test")
            };
            var token = tokenProcessor.Create(claims);

            ClaimsPrincipal principal;
            var isValid = tokenProcessor.Validate(token, DateTime.UtcNow.AddMinutes(1), out principal);

            Assert.AreEqual(true, isValid);
            var claim = principal.FindFirst("Name");

            Assert.AreEqual("test", claim.Value);
        }
    }
}