﻿using System;
using System.Security.Claims;

namespace GekkoEx.Auth
{
    public interface ITokenProcessor
    {
        string Create(Claim[] claims);
        bool Validate(string authToken, DateTime valid, out ClaimsPrincipal principal);
    }
}
