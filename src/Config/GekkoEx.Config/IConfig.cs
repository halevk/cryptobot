﻿using System;

namespace GekkoEx.Config
{
    public interface IConfig
    {
        void Set<T>(string key, T value);
        T Get<T>(string key);
    }
}
