﻿using Microsoft.Extensions.Configuration;
using System;

namespace GekkoEx.Config.App
{
    public class AppConfig : IConfig
    {
        IConfiguration config;
        public AppConfig(IConfiguration config)
        {
            this.config = config;
        }
        public T Get<T>(string key)
        {
            return config.GetValue<T>(key);
        }

        public void Set<T>(string key, T value)
        {
            config.GetSection(key).Value = value.ToString();
        }
    }
}
