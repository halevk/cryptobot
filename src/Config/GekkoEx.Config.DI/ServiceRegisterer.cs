﻿using GekkoEx.Config.App;
using GekkoEx.DI;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace GekkoEx.Config.DI
{
    public class ServiceRegisterer : IServiceRegisterer
    {
        public void Register(IServiceCollection collection)
        {
            collection.AddSingleton<IConfig, AppConfig>();
        }
    }
}
