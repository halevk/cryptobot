﻿using GekkoXe.CloudProvider.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GekkoXe.CloudProvider.DOcean.Test
{
    public static class TestExtensions
    {
        public static string[] GetInstanceIds(this InstanceInfo[] infos)
        {
            if (!infos.Any()) return new string[0];
            return infos.Select(p => p.Id).ToArray();
        }
    }
}
