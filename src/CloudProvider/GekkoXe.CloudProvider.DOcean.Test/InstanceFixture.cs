using FluentAssertions;
using GekkoXe.CloudProvider;
using GekkoXe.CloudProvider.DOcean;
using GekkoXe.CloudProvider.DOcean.Test;
using GekkoXe.CloudProvider.Models;
using NUnit.Framework;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Tests
{
    public class InstanceFixture
    {
        ICloudInstanceProvider provider;

        [SetUp]
        public void Setup()
        {
            provider = new DigitalOceanProvider("");
        }

        [Test]
        public async Task ShouldListInstances()
        {
            var instances = await GetInstances();
            instances.Length.Should().BeGreaterThan(0);
        }

        [Test]
        public async Task ShouldGetInstance()
        {
            var instances = await GetInstances();
            var instanceInfos = await provider.GetInstanceInfo(instances.Select(o => o.Id).ToArray());
            instanceInfos.Length.Should().BeGreaterThan(0);
        }

        [Test]
        public async Task ShouldGetInstanceStatus()
        {
            var instances = await GetInstances();
            var statuses = await provider.GetStatus(instances.Select(p => p.Id).ToArray());
            statuses.Length.Should().BeGreaterThan(0);            
        }

        [Test]
        public async Task ShouldStartInstance()
        {
            var instances = await GetInstances();
            try { await provider.Start(instances.GetInstanceIds()); }
            catch(Exception ex) { throw ex; }
            
        }

        [Test]
        public async Task ShouldStopInstance()
        {
            var instances = await GetInstances();
            try { await provider.Stop(instances.GetInstanceIds()); }
            catch (Exception ex) { throw ex; }
        }


        [Test]
        public async Task ShouldRunNewInstance()
        {
            var options = new RunInstanceOptionsForDigitalOcean {
                 
            };
            var result = await provider.Run(options);
            result.Should().NotBeNull();
        }

        private async Task<InstanceInfo[]> GetInstances()
        {
            var instances = await provider.ListInstaces();
            if (instances.Length == 0) throw new System.Exception("no instance found");
            return instances;
        }
    }
}