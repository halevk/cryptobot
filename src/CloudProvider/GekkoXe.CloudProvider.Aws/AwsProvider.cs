﻿using Amazon;
using Amazon.EC2;
using Amazon.EC2.Model;
using Amazon.Runtime;
using GekkoXe.CloudProvider.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GekkoXe.CloudProvider.Aws
{
    public class AwsProvider : ICloudInstanceProvider
    {
        AmazonEC2Client client;
        public AwsProvider(string accessKey, string secretKey,string region)
        {
            
               client = new AmazonEC2Client(new BasicAWSCredentials(accessKey, secretKey), RegionEndpoint.GetBySystemName(region));
        }

        public async Task<InstanceInfo[]> GetInstanceInfo(string[] instanceIds)
        {
            var result = await MakeRequest((client) => client.DescribeInstancesAsync(new DescribeInstancesRequest { InstanceIds = instanceIds.ToList() }));
            if (result == null) return new InstanceInfo[0];
            return result.Reservations
                .SelectMany(p => p.Instances.Select(o => MapInstance(o)))
                .Where(o => o != null)
                .ToArray();            
        }

        private static InstanceInfo MapInstance(Instance instance)
        {
            if (instance == null) return null;
            return new InstanceInfo
            {
                Id = instance.InstanceId,
                PublicIp = instance.PublicIpAddress,
                PrivateIp = instance.PrivateIpAddress,
                Name = "",
                Status = instance.State.Name,
                CoreCount = instance.CpuOptions.CoreCount,
                ImageId = instance.ImageId,
                DiskSize = "",
                InstanceType = instance.InstanceType,
                RamSize = instance.RamdiskId,
                Platform = instance.Architecture.Value
            };
        }

        public async Task<string[]> GetStatus(string[] instanceIds)
        {
            var result = await GetInstanceInfo(instanceIds);
            return result.Select(p => p.Status).ToArray();
        }

        public async Task<InstanceInfo[]> ListInstaces()
        {
            var result = await MakeRequest((client) => client.DescribeInstancesAsync(new DescribeInstancesRequest()));
            if (result == null) return new InstanceInfo[0];
            return result.Reservations
                .SelectMany(p => p.Instances.Select(o => MapInstance(o)))
                .Where(o => o != null)
                .ToArray();
        }

        public async Task Start(string[] instanceIds)
        {
            await MakeRequest((client) => client.StartInstancesAsync(new StartInstancesRequest { InstanceIds = instanceIds.ToList() }));
        }

        public async Task Stop(string[] instanceIds)
        {
            await MakeRequest((client) => client.StopInstancesAsync(new StopInstancesRequest { InstanceIds = instanceIds.ToList() }));
        }

        public async Task Terminate(string[] instanceIds)
        {
            await MakeRequest((client) => client.TerminateInstancesAsync(new TerminateInstancesRequest { InstanceIds = instanceIds.ToList() }));
        }

        private async Task<T> MakeRequest<T>(Func<AmazonEC2Client, Task<T>> fn)
        {
            try
            {
                return await fn(client);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<InstanceInfo> Run(object options)
        {
            if (options.GetType() != typeof(RunInstanceOptionsForAws))
                throw new ArgumentException("option not supported");
            var request = (RunInstanceOptionsForAws)options;
            var launchRequest = new RunInstancesRequest()
            {
                ImageId = request.ImageId,
                InstanceType = request.InstanceType,
                MinCount = request.MinCount,
                MaxCount = request.MaxCount,
                KeyName = request.KeyPairName,
                SecurityGroupIds = request.SecurityGroupIds
            };
            var response = await MakeRequest((client) => client.RunInstancesAsync(launchRequest));
            return MapInstance(response.Reservation.Instances.FirstOrDefault());
        }
    }
}
