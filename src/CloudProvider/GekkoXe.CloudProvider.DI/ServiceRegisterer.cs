﻿using GekkoEx.Config;
using GekkoEx.DI;
using GekkoXe.CloudProvider.Aws;
using GekkoXe.CloudProvider.DOcean;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace GekkoXe.CloudProvider.DI
{
    public class ServiceRegisterer : IServiceRegisterer
    {
        public void Register(IServiceCollection collection)
        {
            collection.AddSingleton<ICloudInstanceProviderFactory>(p =>
                 new CloudInstanceProviderFactory(p.GetService<IConfig>())
            );            
        }       
    }

    public class CloudInstanceProviderFactory : ICloudInstanceProviderFactory
    {
        IConfig config;
        public CloudInstanceProviderFactory(IConfig config)
        {
            this.config = config;
        }
        public ICloudInstanceProvider Create(CloudProvider provider)
        {   
            if (provider == CloudProvider.Aws)
                return new AwsProvider(config.Get<string>("aws:accessKey"), config.Get<string>("aws:secretKey"), config.Get<string>("aws:region"));
            if (provider == CloudProvider.DigitalOcean)
                return new DigitalOceanProvider(config.Get<string>("digitalocean:token"));
            throw new ArgumentException("not supported cloud provider");
        }
    }
}
