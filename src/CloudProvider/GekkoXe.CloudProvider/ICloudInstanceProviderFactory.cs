﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GekkoXe.CloudProvider
{
    public enum CloudProvider
    {
        Aws,
        DigitalOcean
    }

    public interface ICloudInstanceProviderFactory
    {
        ICloudInstanceProvider Create(CloudProvider provider);
    }
}
