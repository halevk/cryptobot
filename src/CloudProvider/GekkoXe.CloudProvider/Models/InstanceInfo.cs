﻿using System.Collections.Generic;
namespace GekkoXe.CloudProvider.Models
{
    public class InstanceInfo
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Status { get; set; }
        public string PublicIp { get; set; }
        public string PrivateIp { get; set; }
        public string ImageId { get; set; }
        public string RamSize { get; set; }
        public string DiskSize { get; set; }
        public int CoreCount { get; set; }
        public string InstanceType { get; set; }
        public string Platform { get; set; }
        public string CreatedAt { get; set; }
        public Dictionary<string,string> Features { get; set; }
    }
}
