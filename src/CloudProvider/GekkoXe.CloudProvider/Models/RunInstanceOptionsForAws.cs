﻿using System.Collections.Generic;

namespace GekkoXe.CloudProvider.Models
{
    public class RunInstanceOptionsForAws
    {
        public string ImageId { get; set; }
        public string InstanceType { get; set; }
        public int MinCount { get; set; }
        public int MaxCount { get; set; }
        public string KeyPairName { get; set; }
        public List<string> SecurityGroupIds { get; set; }
    }
}
