﻿using GekkoXe.CloudProvider.Models;
using System.Threading.Tasks;

namespace GekkoXe.CloudProvider
{
    public interface ICloudInstanceProvider
    {
        Task Start(string[] instanceIds);
        Task Stop(string[] instanceIds);
        Task Terminate(string[] instanceIds);
        Task<InstanceInfo[]> ListInstaces();
        Task<InstanceInfo[]> GetInstanceInfo(string[] instanceIds);
        Task<InstanceInfo> Run(object options);
        Task<string[]> GetStatus(string[] instanceIds);
    }
}
