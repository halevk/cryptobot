﻿using DigitalOcean.API;
using DigitalOcean.API.Models.Responses;
using GekkoXe.CloudProvider.Models;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace GekkoXe.CloudProvider.DOcean
{
    public class DigitalOceanProvider : ICloudInstanceProvider
    {
        string apiToken;
        DigitalOceanClient client;
        public DigitalOceanProvider(string apiToken)
        {
            client = new DigitalOceanClient(apiToken);
        }

        public async Task<InstanceInfo[]> GetInstanceInfo(string[] instanceIds)
        {
            var tasks = instanceIds.Select(p => client.Droplets.Get(int.Parse(p)));
            await Task.WhenAll(tasks);
            return tasks.Select(p => Map(p.Result)).Where(p => p != null).ToArray();
        }

        public async Task<string[]> GetStatus(string[] instanceIds)
        {
            var tasks = instanceIds.Select(o => client.Droplets.Get(int.Parse(o)));
            await Task.WhenAll(tasks);
            return tasks.Select(p => p.Result.Status).ToArray();
        }

        public async Task<InstanceInfo[]> ListInstaces()
        {
            var result = await client.Droplets.GetAll();
            return result.Select(o => Map(o)).Where(p => p != null).ToArray();
        }

        public async Task<InstanceInfo> Run(object options)
        {
            if (options.GetType() != typeof(RunInstanceOptionsForDigitalOcean))
                throw new ArgumentException("option not supported");
            var request = (RunInstanceOptionsForDigitalOcean)options;

            var newDroplet = new DigitalOcean.API.Models.Requests.Droplet
            {
               Name="",
               ImageIdOrSlug=null,
               SizeSlug="",
               SshIdsOrFingerprints=null
            };
            var result = await client.Droplets.Create(newDroplet);
            return Map(result);
        }

        public async Task Start(string[] instanceIds)
        {
            var tasks = instanceIds.Select(p => client.DropletActions.PowerOn(int.Parse(p)));
            await Task.WhenAll(tasks);
        }

        public async Task Stop(string[] instanceIds)
        {
            var tasks = instanceIds.Select(p => client.DropletActions.PowerOff(int.Parse(p)));
            await Task.WhenAll(tasks);
        }

        public async Task Terminate(string[] instanceIds)
        {
            var tasks = instanceIds.Select(p => client.Droplets.Delete(int.Parse(p)));
            await Task.WhenAll(tasks);
        }

        private InstanceInfo Map(Droplet droplet)
        {
            if (droplet == null) return new InstanceInfo();
            return new InstanceInfo
            {
                Id = droplet.Id.ToString(),
                Name = droplet.Name,
                RamSize = droplet.Memory.ToString(),
                ImageId = droplet.Image.Slug,
                CoreCount = droplet.Vcpus,
                DiskSize = droplet.Disk.ToString(),
                InstanceType = droplet.Image.Type,
                Platform = droplet.Kernel.Name,
                PublicIp = droplet.Networks.v4.First().IpAddress,
                Status = droplet.Status
            };
        }

    }
}
