using FluentAssertions;
using GekkoXe.CloudProvider;
using GekkoXe.CloudProvider.Aws;
using GekkoXe.CloudProvider.Models;
using NUnit.Framework;
using System.Linq;
using System.Threading.Tasks;

namespace Tests
{
    public class InstanceFixture
    {
        ICloudInstanceProvider provider;

        [SetUp]
        public void Setup()
        {            
               provider = new AwsProvider("", "","eu-central-1");
        }

        [Test]
        public async Task ShouldListInstances()
        {
            var instances = await provider.ListInstaces();
            instances.Length.Should().BeGreaterThan(0);
        }

        [Test]
        public async Task ShouldGetInstance()
        {
            var instances = await GetInstances();
            var instanceInfo =  await provider.GetInstanceInfo(instances.Select(p=> p.Id).ToArray());
            instanceInfo.Should().NotBeNull();
        }

        [Test]
        public async Task ShouldGetInstanceStatus()
        {
            var instances = await GetInstances();
            var stats = await provider.GetStatus(instances.Select(p => p.Id).ToArray());
            stats.Length.Should().BeGreaterThan(0);
        }

        [Test]
        public async Task ShouldStartInstance()
        {
            var instances = await GetInstances();
            try
            {
                await provider.Start(instances.Select(p => p.Id).ToArray());
            }
            catch(System.Exception ex) { throw ex; }
        }

        [Test]
        public async Task ShouldStopInstance()
        {
            var instances = await GetInstances();
            try
            {
                await provider.Stop(instances.Select(p => p.Id).ToArray());
            }
            catch (System.Exception ex) { throw ex; }
        }

        [TestCase("",new[] {"" })]
        public async Task ShouldRunNewInstance(string keyPairName,string[] securityGroupIds)
        {
            if (string.IsNullOrWhiteSpace(keyPairName))
                throw new System.Exception("keypairname is required");
            if (securityGroupIds.Length == 0)
                throw new System.Exception("securityGroupIds are required");
            var option = new RunInstanceOptionsForAws
            {
                ImageId = "ami-090f10efc254eaf55",
                InstanceType = "t2.nano",
                KeyPairName = keyPairName,
                MaxCount = 1,
                MinCount = 1,
                SecurityGroupIds = securityGroupIds.ToList()
            };
            var result = await provider.Run(option);
        }

        private async Task<InstanceInfo[]> GetInstances()
        {
            var instances = await provider.ListInstaces();
            if (instances.Length == 0) throw new System.Exception("no instance found");
            return instances;
        }
    }
}