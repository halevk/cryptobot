﻿using System;
using System.Threading.Tasks;

namespace GekkoXe.Wallet
{
    public interface IWallet
    {
        Task<string> Create();
        Task<double> GetBalance(string walletId);
        Task<string> Transfer(string senderWalletId, string receiverWalletId, double amount);
        Task GetTransactionHistory(string walletId);
    }
}
