﻿using GekkoEx.Messaging;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;

namespace GekkoEx.Api
{
    public class AppContext : HttpContextAccessor, IAppContext
    {    
        public void Create(HttpContext context)
        {
            if(this.HttpContext==null)
                base.HttpContext = context;
        }
        public void Add(object key, object value)
        {
            base.HttpContext.Items.TryAdd(key, value);               
        }

        public void AddRange(IDictionary<object, object> items)
        {
            foreach (var itm in items)
                if (!base.HttpContext.Items.ContainsKey(itm.Key))
                    base.HttpContext.Items.Add(itm.Key, itm.Value);
        }

        public T Get<T>(object key)
        {
            object value;
            if (base.HttpContext.Items.TryGetValue(key, out value))
                return (T)value;
            return default(T);
        }
    }
}
