﻿using GekkoEx.Messaging;
using System;
using System.Collections.Generic;

namespace GekkoEx.Api
{
    public class RouteRegistry : IRouteRegistry
    {
        Dictionary<string, Tuple<Type,bool>> routes;
        public RouteRegistry()
        {
            routes = new Dictionary<string, Tuple<Type,bool>>();
        }        

        public Type GetRouteType(string path)
        {
            Tuple<Type,bool> data;
            if (routes.TryGetValue(path, out data))
                return data.Item1;
            return null;
        }

        public bool IsLogin(string path)
        {            
            Tuple<Type, bool> data;
            if (routes.TryGetValue(path, out data))
                return data.Item2;

            return false;
        }

        public void Register(string path, Tuple<Type,bool> model)
        {
            routes.Add(path, model);
        }
      
    }
}
