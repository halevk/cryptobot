﻿using GekkoEx.DI;
using GekkoEx.Messaging;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace GekkoEx.Api
{
    public class ModulRegistry
    {
        private static Assembly[] assemblies;
        public static void RegisterServices(IServiceCollection collection)
        {
            var libs = GetAssemblies().Where(p => p.FullName.Contains(".DI"));
            foreach (var lib in libs)
            {
                var registerer = lib.GetTypes().FirstOrDefault(p => !p.IsInterface && typeof(IServiceRegisterer).IsAssignableFrom(p));
                if (registerer != null)
                    ((IServiceRegisterer)Activator.CreateInstance(registerer)).Register(collection);
            }
        }

        public static void RegisterRoutes(IRouteRegistry routeRegistry)
        {
            var libraries = GetAssemblies().Where(p => p.FullName.Contains(".Message"));
            foreach (var library in libraries)
            {
                foreach (var typ in library.GetTypes())
                {
                    foreach (var i in typ.GetInterfaces())
                    {
                         if(i.IsGenericType && i.GetGenericTypeDefinition() == typeof(IRequest<>))
                        {
                            foreach (var attr in typ.GetCustomAttributes(typeof(RouteHandlerAttribute)))
                            {
                                var routeAttr = attr as RouteHandlerAttribute;
                                routeRegistry.Register($"{routeAttr.Modul}/{routeAttr.Route}", new Tuple<Type, bool>(typ, routeAttr.IsLogin));
                            }
                        }
                    }
                }               
            }
        }

        public static void RegisterHandlers(IServiceCollection collection)
        {
            var libs = GetAssemblies().Where(p => p.FullName.Contains(".Handler"));
            foreach (var lib in libs)
            {
                foreach (var typ in lib.GetTypes())
                {
                    RegisterGenerics(typ, typeof(IRequestHandler<,>), t=> collection.AddTransient(t.GetGenericArguments().First(),typ) );
                    RegisterGenerics(typ, typeof(IEventHandler<>), t=> {
                        var interfaceType = typeof(IEventHandler<>).MakeGenericType(t.GetGenericArguments());
                        collection.AddTransient(interfaceType, typ);
                    });
                }
            }
        }
       
        private static Action<Type, Type, Action<Type>> RegisterGenerics = (typ, handlerType, matchFunc) =>
        {
            foreach (var i in typ.GetInterfaces())            
                if (i.IsGenericType && i.GetGenericTypeDefinition() == handlerType)
                    matchFunc(i);        
        };


        private static Assembly[] GetAssemblies()
        {
            if (assemblies == null)
            {
                var asms = new List<Assembly>();
                var libraries = DependencyContext.Default.RuntimeLibraries;
                foreach (var library in libraries)
                {
                    if (IsCompilationLibrary(library))
                    {
                        asms.Add(Assembly.Load(new AssemblyName(library.Name)));
                    }
                }
                assemblies = asms.ToArray();
            }
            return assemblies;
        }

        private static bool IsCompilationLibrary(RuntimeLibrary library)
        {
            return library.Name.StartsWith("GekkoEx") || library.Dependencies.Any(p => p.Name.StartsWith("GekkoEx"));
        }
    }
}
