﻿using GekkoEx.Api.Extensions;
using GekkoEx.Messaging;
using GekkoEx.Messaging.Message.Commands;
using Microsoft.AspNetCore.Http;
using System;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace GekkoEx.Api.Middleware
{
    public class AuthMiddleware
    {
        RequestDelegate next;
        IRouteRegistry routeRegistry;
        IMessageBroker broker;        
        public AuthMiddleware(RequestDelegate next, IRouteRegistry routeRegistry,IMessageBroker broker)
        {
            this.next = next;
            this.routeRegistry = routeRegistry;
            this.broker = broker;            
        }

        public async Task Invoke(HttpContext context)
        {
            var path = context.Request.Path.Value.Trim('/');
            if (routeRegistry.IsLogin(path))
            {
                await HandleLogin(path, context,new CancellationToken());                            
                return;
            }

            var authenticationType = "bearer";
            string authToken = "";
            if(!context.CheckAuthorizationHeaderExists(authenticationType,out authToken))
            {
                await context.WriteError(HttpStatusCode.Unauthorized, new Exception("Unauthorize access"));
                return;
            }

            var validationResult = await ValidateTokenSetPrincipal(authToken, new CancellationToken());
            if (!validationResult)
            {
                await context.WriteError(HttpStatusCode.Unauthorized, new Exception("Unauthorize access"));
                return;
            }

            await next(context);
        }

        private async Task<bool> ValidateTokenSetPrincipal(string authToken,CancellationToken cancellationToken)
        {
            var result = await broker.Send(new ValidateToken { Token = authToken }, cancellationToken);
            if (result.Status)
                Thread.CurrentPrincipal = result.Principal;
            return result.Status;
        }

        private async Task HandleLogin(string path,HttpContext context,CancellationToken token)
        {            
            try
            {
                var request = (AuthorizeUser)context.GetRequestObject(routeRegistry, path);
                var result = await broker.Send(request, token);
                await context.WriteResult(result);
            }
            catch (ValidationException ex) { await context.WriteError(HttpStatusCode.BadRequest, ex, token); }
            catch (Exception ex) { await context.WriteError(HttpStatusCode.InternalServerError, ex, token); }
        }        
    }
}
