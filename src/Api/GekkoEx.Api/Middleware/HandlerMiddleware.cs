﻿using GekkoEx.Api;
using GekkoEx.Api.Extensions;
using GekkoEx.Messaging;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Primitives;
using System;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace GekkoEx.Api.Middleware
{
    public class HandlerMiddleware
    {
        RequestDelegate next;
        IMessageBroker broker;
        IRouteRegistry routeRegistry;      
        IAppContext appContext;
        public HandlerMiddleware(RequestDelegate next, IMessageBroker broker, IRouteRegistry routeRegistry, IAppContext appContext)
        {
            this.next = next;
            this.broker = broker;
            this.routeRegistry = routeRegistry;            
            this.appContext = appContext;
        }

        public async Task Invoke(HttpContext context)
        {           
            var token = new CancellationToken();
            try
            {
                var requestId = appContext.Get<string>("requestId");
                var request = context.GetRequestObject(routeRegistry);
                context.Response.Headers.Add("requestId", new StringValues(requestId));
                var result = await Send(request, token, broker);
                await context.WriteResult(result, token);
            }
            catch (ValidationException ex)
            {
                await context.WriteError(HttpStatusCode.BadRequest, ex, token);
            }
            catch (Exception ex)
            {
                await context.WriteError(HttpStatusCode.InternalServerError, ex, token);
            }
        }

        private async Task<object> Send(object request, CancellationToken token, IMessageBroker messageBroker)
        {
            var requestType = request
               .GetType()
               .GetInterfaces()
               .Where(x => x.IsGenericType)
               .FirstOrDefault(x => x.GetGenericTypeDefinition() == typeof(IRequest<>));

            if (requestType == null)
                throw new Exception("Message dont implement IRequest<>");

            var responseType = requestType.GetGenericArguments().First();
            var proxy = (MessageBrokerProxy)Activator.CreateInstance(typeof(MessageBrokerProxy<>).MakeGenericType(responseType), messageBroker);
            return await proxy.Call(request, token);
        }      

             
    }
}
