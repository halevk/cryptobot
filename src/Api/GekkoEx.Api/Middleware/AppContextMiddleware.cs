﻿using GekkoEx.Messaging;
using Microsoft.AspNetCore.Http;
using System;
using System.Threading.Tasks;

namespace GekkoEx.Api.Middleware
{

    public class AppContextMiddleware
    {
        RequestDelegate next;
        IAppContext appContext;
        public AppContextMiddleware(RequestDelegate next,IAppContext context)
        {
            this.next = next;
            this.appContext = context;
        }

        public async Task Invoke(HttpContext context)
        {
            ((AppContext)appContext).Create(context);
            var requestId = Guid.NewGuid().ToString();
            appContext.Add("requestId", requestId);
            await next(context);
        }
    }
}
