﻿using GekkoEx.Messaging;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;
using System.IO;

namespace GekkoEx.Api.Extensions
{
    public static class RequestExtensions
    {
        public static object GetRequestObject(this HttpContext context,IRouteRegistry routeRegistry)
        {
            var path = context.Request.Path.Value.Trim('/');
            var reqType = routeRegistry.GetRouteType(path);
            using (var sr = new StreamReader(context.Request.Body))
                return JsonConvert.DeserializeObject(sr.ReadToEnd(), reqType);           
        }

        public static object GetRequestObject(this HttpContext context, IRouteRegistry routeRegistry,string path)
        {            
            var reqType = routeRegistry.GetRouteType(path);
            using (var sr = new StreamReader(context.Request.Body))
                return JsonConvert.DeserializeObject(sr.ReadToEnd(), reqType);
        }

        public static bool CheckAuthorizationHeaderExists(this HttpContext context,string authenticationType,out string authToken)
        {
            var authInfo = context.Request.Headers["Authorization"];
            var exists = authInfo.Count == 1 &&
                !string.IsNullOrWhiteSpace(authInfo[0]) &&
                authInfo[0].StartsWith(authenticationType, StringComparison.InvariantCultureIgnoreCase);
            if (!exists)
            {
                authToken = string.Empty;
                return false;
            }
            var items = authInfo[0].Trim().Split(' ');
            authToken = items[1].Trim();
            return true;
        }
    }
}
