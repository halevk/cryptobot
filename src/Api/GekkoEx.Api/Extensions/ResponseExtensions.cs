﻿using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace GekkoEx.Api.Extensions
{
    public static class ResponseExtensions
    {
        public static async Task WriteError(this HttpContext context, 
            HttpStatusCode httpStatus, 
            Exception ex, 
            CancellationToken token=default(CancellationToken))
        {
            context.Response.StatusCode = (int)httpStatus;
            await context.Response.WriteAsync(ex.Message, token);
        }

        public static async Task WriteResult<T>(this HttpContext context, 
            T data,
            CancellationToken token = default(CancellationToken),
            HttpStatusCode httpStatus = HttpStatusCode.OK,              
            string contentType = "application/json")
        {
            context.Response.ContentType = contentType;
            context.Response.StatusCode = (int)httpStatus;
            await context.Response.WriteAsync(JsonConvert.SerializeObject(data), token);
        }
    }
}
