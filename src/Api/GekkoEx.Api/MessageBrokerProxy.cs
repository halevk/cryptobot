﻿using GekkoEx.Messaging;
using System.Threading;
using System.Threading.Tasks;

namespace GekkoEx.Api
{
    internal abstract class MessageBrokerProxy
    {
        public abstract Task<object> Call(object request, CancellationToken token);
    }
    internal class MessageBrokerProxy<T> : MessageBrokerProxy
    {
        IMessageBroker broker;
        public MessageBrokerProxy(IMessageBroker broker)
        {
            this.broker = broker;
        }

        public override async Task<object> Call(object request,CancellationToken token)
        {
            return await broker.Send((IRequest<T>)request, token);
        }
    }
}
