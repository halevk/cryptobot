﻿using System;
using System.Net;
using GekkoEx.Api.Middleware;
using GekkoEx.Messaging;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace GekkoEx.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton<IAppContext, AppContext>();
            ModulRegistry.RegisterServices(services);
            ModulRegistry.RegisterHandlers(services);
            services.AddSingleton<IRouteRegistry, RouteRegistry>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, IServiceProvider provider)
        {
            var isAuthEnabled = Environment.GetEnvironmentVariable("AUTH_ENABLED") == "true";
            ModulRegistry.RegisterRoutes(provider.GetService<IRouteRegistry>());
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.Use(async (context, next) => {
                if (context.Request.Method.ToLower() == "get" &&
                    context.Request.Path.Value.Trim() == "/version")
                {
                    context.Response.StatusCode = (int)HttpStatusCode.OK;
                    var version = typeof(Startup).Assembly.GetName().Version.ToString();
                    await context.Response.WriteAsync($"version :{version}");
                    return;
                }
                await next.Invoke();
            });

            app.Use(async (context, next) => {
                if (context.Request.Method.ToLower() != "post" ||
                    context.Request.ContentType != "application/json")
                {
                    context.Response.StatusCode = (int)HttpStatusCode.BadRequest;
                    return;
                }
                await next.Invoke();
            });
            app.UseMiddleware<AppContextMiddleware>();
            if (isAuthEnabled)
                app.UseMiddleware<AuthMiddleware>();
            app.UseMiddleware<HandlerMiddleware>();
            app.Run(async (context) => await context.Response.WriteAsync("working"));
        }
    }
}
