﻿using GekkoEx.Cache.Redis;
using GekkoEx.Config;
using GekkoEx.DI;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace GekkoEx.Cache.DI
{
    public class ServiceRegisterer : IServiceRegisterer
    {
        public void Register(IServiceCollection collection)
        {
            collection.AddSingleton<ICache>((provider) => {
                var config = provider.Resolve<IConfig>();
                return new AppCache(config.Get<string>("cache:redis:url"));
            });
        }
    }
}
