﻿using StackExchange.Redis;
using System;
using System.Threading.Tasks;

namespace GekkoEx.Cache.Redis
{
    public class AppCache : ICache
    {
        public string Name => "redis";

        ConnectionMultiplexer cache;
        IDatabase db;
        public AppCache(string conf)
        {
            cache = ConnectionMultiplexer.Connect(conf);
            db = cache.GetDatabase();
        }

        Func<object, RedisKey> getKey = (obj) => (RedisKey)obj;
        Func<object, RedisValue> getValue = (obj) => (RedisValue)obj;

        public async Task<bool> Set<K>(K key, object data, TimeSpan? timespan)
        {
            return await db.StringSetAsync(getKey(key), getValue(data), timespan);
        }

        public async Task<object> Get<K>(K key)
        {
            return await db.StringGetAsync(getKey(key));
        }

        public async Task<V> Get<K, V>(K key)
        {
            var result = await db.StringGetAsync(getKey(key));
            return (V)(object)result;
        }

    }
}
