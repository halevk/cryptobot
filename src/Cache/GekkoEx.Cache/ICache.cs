﻿using System;
using System.Threading.Tasks;

namespace GekkoEx.Cache
{
    public interface ICache
    {
        string Name { get; }
        Task<bool> Set<K>(K key, object data, TimeSpan? timespan);
        Task<object> Get<K>(K key);
        Task<V> Get<K, V>(K key);
    }
}
