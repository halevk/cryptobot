﻿using GekkoEx.DI;
using GekkoEx.Repository.Mongo;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace GekkoEx.Repository.DI
{
    public class ServiceRegisterer : IServiceRegisterer
    {
        public void Register(IServiceCollection collection)
        {
            collection.AddSingleton<IStorageFactory, StorageFactory>();
        }
    }
}
