﻿using GekkoEx.Config;

namespace GekkoEx.Repository.Mongo
{
    public class StorageFactory : IStorageFactory
    {
        IConfig config;
        public StorageFactory(IConfig config)
        {
            this.config = config;
        }
        public IStorage<T> New<T>(StorageTypes types) where T : EntityBase
        {
            var mongoUrl = config.Get<string>("dblog:mongo");
            if (types == StorageTypes.Content)
                mongoUrl = config.Get<string>("dbcontent:mongo");
            return new MongoStorage<T>(mongoUrl);
        }
    }
}
