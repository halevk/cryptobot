﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;

namespace GekkoEx.Repository.Mongo
{
    public class MongoStorage<T> : IStorage<T> where T : EntityBase
    {
        IMongoDatabase db;
        public MongoStorage(string mongoUrl)
        {                        
            var url = new MongoUrl(mongoUrl);            
            var client = new MongoClient(url);
            db = client.GetDatabase(url.DatabaseName);
        }

        private IMongoCollection<T> GetCollection()
        {
            return db.GetCollection<T>(typeof(T).Name);
        }
                     
        public async Task<bool> InsertAsync(T data, CancellationToken token = default(CancellationToken))           
        {
            await GetCollection().InsertOneAsync(data, null, token);            
            return true;
        }

        public async Task<bool> UpdateAsync(T data, CancellationToken token = default(CancellationToken))            
        {            
            var result = await GetCollection().ReplaceOneAsync(Builders<T>.Filter.Eq("_id", data.Id), data, null, token);
            return result.IsAcknowledged;
        }
        

        public async Task<bool> DeleteAsync(T data, CancellationToken token = default(CancellationToken))
        {            
            var result = await GetCollection().DeleteOneAsync(Builders<T>.Filter.Eq("_id", data.Id), null, token);
            return result.IsAcknowledged;
        }        

        public async Task<List<T>> FindAsync(Expression<Func<T, bool>> query, CancellationToken token = default(CancellationToken))
        {
            var cursor = await GetCollection().FindAsync(query, null, token);
            return await cursor.ToListAsync();
        }
    }
}
