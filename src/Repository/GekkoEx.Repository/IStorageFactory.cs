﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GekkoEx.Repository
{
    public interface IStorageFactory
    {
        IStorage<T> New<T>(StorageTypes types) where T : EntityBase;
    }
}
