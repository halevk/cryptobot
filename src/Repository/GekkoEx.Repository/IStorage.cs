﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;

namespace GekkoEx.Repository
{
    public interface IStorage<T> where T : EntityBase
    {
        Task<bool> InsertAsync(T data, CancellationToken token = default(CancellationToken));
        Task<bool> UpdateAsync(T data, CancellationToken token = default(CancellationToken));
        Task<bool> DeleteAsync(T data, CancellationToken token = default(CancellationToken));
        Task<List<T>> FindAsync(Expression<Func<T, bool>> query, CancellationToken token = default(CancellationToken));
    }
}
