﻿using Microsoft.Extensions.DependencyInjection;

namespace GekkoEx.DI
{
    public interface IServiceRegisterer
    {
        void Register(IServiceCollection collection);        
    }
}
