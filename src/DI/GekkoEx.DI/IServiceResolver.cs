﻿using System;
using System.Collections.Generic;

namespace GekkoEx.DI
{
    public interface IServiceResolver : IServiceProvider
    {        
        bool TryResolve<T>(out T type);       
        bool TryResolve<T>(out IEnumerable<T> types);
        bool TryResolve(Type type, out object requestedType);
    }
}
