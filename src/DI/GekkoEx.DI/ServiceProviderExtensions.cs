﻿using System;

namespace GekkoEx.DI
{
    public static class ServiceProviderExtensions
    {
        public static T Resolve<T>(this IServiceProvider serviceProvider)
        {
            return  (T)serviceProvider.GetService(typeof(T));
        }

        public static object Resolve(this IServiceProvider serviceProvider,Type typ)
        {
            return serviceProvider.GetService(typ);
        }
    }
}
