﻿using System;

namespace GekkoEx.Messaging
{
    public interface IRouteRegistry
    {
        bool IsLogin(string path);
        void Register(string path, Tuple<Type, bool> model);          
        Type GetRouteType(string path);
    }
}
