﻿using System.Threading;
using System.Threading.Tasks;

namespace GekkoEx.Messaging
{
    public interface IMessageRouter<R>
    {
        Task<R> Route(IRequest<R> message, CancellationToken token = default(CancellationToken));
    }
}
