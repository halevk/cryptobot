﻿using System.Threading;
using System.Threading.Tasks;

namespace GekkoEx.Messaging
{
    public interface IRequestHandler<T,R> where T: IRequest<R>
    {
        Task<R> Handle(T request, CancellationToken token = default(CancellationToken));
    }
}
