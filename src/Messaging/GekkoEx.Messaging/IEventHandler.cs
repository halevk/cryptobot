﻿using System.Threading;
using System.Threading.Tasks;

namespace GekkoEx.Messaging
{
    public interface IEventHandler<T>
    {
        Task Handle(T data, CancellationToken token = default(CancellationToken));
    }
}
