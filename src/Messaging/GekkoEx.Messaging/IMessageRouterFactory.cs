﻿using System;

namespace GekkoEx.Messaging
{
    public interface IMessageRouterFactory
    {
        IMessageRouter<T> Create<T>(Type message);
    }
}
