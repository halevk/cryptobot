﻿using System.Collections.Generic;

namespace GekkoEx.Messaging
{
    public class ValidationException : System.Exception
    {
        public ValidationException(List<string> errors):base($"Validation Errors :{string.Join("|",errors)}")   {     }
    }
}
