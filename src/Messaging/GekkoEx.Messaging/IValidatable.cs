﻿using System.Collections.Generic;

namespace GekkoEx.Messaging
{
    public interface IValidatable {
        ValidationResult Validate();
    }

    public class ValidationResult
    {
        public bool IsValid { get; set; }
        public List<string> Errors { get; set; }
        public ValidationResult()
        {
            Errors = new List<string>();
        }

        public void AddError(params string[] errors)
        {
            Errors.AddRange(errors);
        }
    }
}
