﻿namespace GekkoEx.Messaging
{
    public interface IRouteRegisterer
    {
        void Register(IRouteRegistry routeRegistry);
    }
}
