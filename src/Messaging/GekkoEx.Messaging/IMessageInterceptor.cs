﻿using System.Threading;
using System.Threading.Tasks;

namespace GekkoEx.Messaging
{
    public interface IMessageInterceptor
    {
        Task<R> Invoke<T, R>(T message, CancellationToken token = default(CancellationToken))
            where T : IRequest<R>;
    }
}
