﻿using System.Collections.Generic;

namespace GekkoEx.Messaging
{
    public interface IAppContext
    {       
        void Add(object key, object value);
        void AddRange(IDictionary<object, object> items);
        T Get<T>(object key);
    }
}
