﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace GekkoEx.Messaging
{
    public interface IMessageBroker
    {
        Task<R> Send<R>(IRequest<R> message, CancellationToken token = default(CancellationToken));       
       
        void Publish<T>(T payload, CancellationToken token = default(CancellationToken));
    }
}
