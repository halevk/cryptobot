﻿using System;

namespace GekkoEx.Messaging
{
    [AttributeUsage(AttributeTargets.Class,AllowMultiple =true)]
    public class RouteHandlerAttribute : Attribute
    {
        public string Modul { get; set; }
        public string Route { get; set; }
        public bool IsLogin { get; set; }
    }
}
