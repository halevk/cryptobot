﻿using GekkoEx.DI;
using System.Threading;
using System.Threading.Tasks;

namespace GekkoEx.Messaging.InMemory
{
    public class ValidationInterceptor : InterceptorBase
    {
        IServiceResolver serviceResolver;       
        public ValidationInterceptor(IServiceResolver serviceResolver, IMessageInterceptor next):base(next)  {
            this.serviceResolver = serviceResolver;
        }
        public async override Task<R> Invoke<T, R>(T message, CancellationToken token = default(CancellationToken))            
        {
            var isValidatable = typeof(IValidatable).IsAssignableFrom(message.GetType());
            if (isValidatable)
            {
                var validationResult = ((IValidatable)message).Validate();
                validationResult.IsValid = validationResult.Errors.Count == 0;
                if (!validationResult.IsValid)
                    throw new ValidationException(validationResult.Errors);
            }
            return await base.next.Invoke<T, R>(message, token);            
        }
    }
}
