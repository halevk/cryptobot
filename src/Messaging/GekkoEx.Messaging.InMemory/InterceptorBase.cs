﻿using System.Threading;
using System.Threading.Tasks;

namespace GekkoEx.Messaging.InMemory
{
    public abstract class InterceptorBase : IMessageInterceptor
    {
        protected IMessageInterceptor next;
        public InterceptorBase(IMessageInterceptor next)
        {
            this.next = next;
        }       
        public abstract Task<R> Invoke<T, R>(T message, CancellationToken token = default(CancellationToken))
            where T : IRequest<R>;
    }
}
