﻿using System;

namespace GekkoEx.Messaging.InMemory
{
    public class Message<T,R> where T: IRequest<R>
    {
        public string User { get; set; }
        public T Request { get; set; }
        public R Response { get; set; }
        public string Route { get; set; }
        public double ExecutionTime { get; set; }
        public Exception Error { get; set; }
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
        public string RequestId { get; set; }
    }
}
