﻿using GekkoEx.DI;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;

namespace GekkoEx.Messaging.InMemory
{
    public class ServiceResolver : IServiceResolver
    {
        IServiceProvider provider;
        public ServiceResolver(IServiceProvider provider)
        {
            this.provider=provider;
        }

        public object GetService(Type serviceType)
        {
            return provider.Resolve(serviceType);
        }

        public bool TryResolve<T>(out IEnumerable<T> types)
        {
            try
            {
                types = provider.GetServices<T>();
                return types.Any();
            }
            catch { }
            types = new T[0];
            return false;
        }

        public bool TryResolve<T>(out T type)
        {
            try
            {
                type = (T)provider.Resolve(typeof(T));
                return type != null;
            }
            catch { }
            type = default(T);
            return false;
        }

        public bool TryResolve(Type type, out object requestedType)
        {
            try
            {
                requestedType = provider.Resolve(type);
                return requestedType != null;
            }
            catch { }
            requestedType = null;
            return false;            
        }       
    }
}
