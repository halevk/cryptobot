﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;
using System;
using System.Security.Claims;
using GekkoEx.Log;

namespace GekkoEx.Messaging.InMemory
{
    public class LogInterceptor : InterceptorBase
    {
        ILogger logger;
        IAppContext appContext;
        public LogInterceptor(IEnumerable<ILogger> logger, IAppContext appContext, IMessageInterceptor next) : base(next)
        {
            this.appContext = appContext;
            this.logger = logger.First();
        }
        public async override Task<R> Invoke<T, R>(T message, CancellationToken token = default(CancellationToken))
        {
            var reqId = appContext.Get<string>("requestId");
            var route = message.GetType().GetCustomAttributes(typeof(RouteHandlerAttribute), false)
                .Select(p => ((RouteHandlerAttribute)p).Route)
                .ToArray();
            var msg = new Message<T, R>
            {
                Start = DateTime.UtcNow,
                Route = route?.FirstOrDefault(),
                User = GetUserName(),
                Request = message,
                RequestId = reqId
            };
            try
            {
                msg.Response = await base.next.Invoke<T, R>(message, token);
            }
            catch (Exception ex) {
                msg.Error = ex;
            }
            msg.End = DateTime.UtcNow;
            msg.ExecutionTime = (msg.End - msg.Start).TotalMilliseconds;
            await logger.Log(msg);
            if (msg.Error != null)
                return default(R);
            return msg.Response;
        }

        private Func<string> GetUserName = () => {
            var principal = ((ClaimsPrincipal)Thread.CurrentPrincipal);
            if (principal == null)
                return string.Empty;
            var userName = principal.FindFirst(p => p.Type == "UserName");
            if (userName == null)
                return string.Empty;
            return userName.Value;
        };
    }
}
