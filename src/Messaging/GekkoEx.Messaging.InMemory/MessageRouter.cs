﻿using GekkoEx.DI;
using System.Threading;
using System.Threading.Tasks;

namespace GekkoEx.Messaging.InMemory
{
    public class MessageRouter<T,R> : IMessageRouter<R> where T : IRequest<R>
    {
        IServiceResolver serviceResolver;
        public MessageRouter(IServiceResolver serviceResolver)
        {
            this.serviceResolver = serviceResolver;
        }
        public async Task<R> Route(IRequest<R> message, CancellationToken token = default(CancellationToken))            
        {
            IMessageInterceptor messageInterceptor;
            if(serviceResolver.TryResolve(out messageInterceptor))                           
                return await messageInterceptor.Invoke<T, R>((T)message, token);            
            return default(R);
        }
    }
}
