﻿using GekkoEx.DI;
using System;
using System.Collections.Concurrent;

namespace GekkoEx.Messaging.InMemory
{
    public class DefaultRouterFactory : IMessageRouterFactory
    {
        IServiceResolver resolver;
        public DefaultRouterFactory(IServiceResolver resolver)
        {
            this.resolver = resolver;
        }
        public IMessageRouter<T> Create<T>(Type message)
        {
            return (IMessageRouter<T>)cache.GetOrAdd(message, x =>
            {
                var routerType = typeof(MessageRouter<,>).MakeGenericType(x, typeof(T));
                return Activator.CreateInstance(routerType, resolver);
            });            
        }

        readonly ConcurrentDictionary<Type, object> cache = new ConcurrentDictionary<Type, object>();
    }
}
