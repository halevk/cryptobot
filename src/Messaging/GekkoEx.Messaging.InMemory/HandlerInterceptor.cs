﻿using GekkoEx.DI;
using System.Threading;
using System.Threading.Tasks;

namespace GekkoEx.Messaging.InMemory
{
    public class HandlerInterceptor : InterceptorBase
    {
        IServiceResolver serviceResolver;
        public HandlerInterceptor(IServiceResolver serviceResolver,IMessageInterceptor next):base(next)
        {
            this.serviceResolver = serviceResolver;
        }
        public async override Task<R> Invoke<T, R>(T message, CancellationToken token = default(CancellationToken))
        {            
            object handler;            
            var isResolved = serviceResolver.TryResolve(message.GetType(), out handler);
            if (isResolved)
                return await ((IRequestHandler<T, R>)handler).Handle(message, token);
            return await base.next.Invoke<T, R>(message, token);
        }
    }
}
