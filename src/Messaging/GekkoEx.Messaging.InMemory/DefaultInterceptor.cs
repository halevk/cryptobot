﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace GekkoEx.Messaging.InMemory
{
    public class DefaultInterceptor : IMessageInterceptor
    {
        public async Task<R> Invoke<T, R>(T message, CancellationToken token = default(CancellationToken)) 
            where T : IRequest<R>
        {
            return await Task.FromException<R>(new Exception("handler not found"));
        }
    }
}
