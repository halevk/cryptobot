﻿using GekkoEx.DI;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace GekkoEx.Messaging.InMemory
{
    public class InMemoryBroker : IMessageBroker
    {        
        IMessageRouterFactory messageRouterFactory;
        IServiceResolver serviceResolver;
        public InMemoryBroker(IMessageRouterFactory messageRouterFactory, IServiceResolver serviceResolver)
        {
            this.serviceResolver = serviceResolver;
            this.messageRouterFactory = messageRouterFactory;           
        }
        public async void Publish<T>(T payload, CancellationToken token = default(CancellationToken))
        {
            IEnumerable<IEventHandler<T>> subs;
            if (serviceResolver.TryResolve(out subs))
                await Task.WhenAny(subs.Select(p => p.Handle(payload, token)));
        }

        public async Task<R> Send<R>(IRequest<R> message, CancellationToken token = default(CancellationToken))             
        {
            var router = messageRouterFactory.Create<R>(message.GetType());
            return await router.Route(message, token);
        }               
    }
}
