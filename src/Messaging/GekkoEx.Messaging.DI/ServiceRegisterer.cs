﻿using GekkoEx.DI;
using GekkoEx.Log;
using GekkoEx.Messaging.InMemory;
using Microsoft.Extensions.DependencyInjection;
using System.Collections.Generic;

namespace GekkoEx.Messaging.DI
{
    public class ServiceRegisterer : IServiceRegisterer
    {
        public void Register(IServiceCollection collection)
        {
            collection.AddTransient<IMessageRouterFactory>((provider) => new DefaultRouterFactory(provider.Resolve<IServiceResolver>()));
            collection.AddTransient<IServiceResolver, ServiceResolver>();
            collection.AddTransient<IMessageInterceptor>((provider) =>
            {                
                IMessageInterceptor interceptor = new DefaultInterceptor();
                interceptor = new HandlerInterceptor(provider.Resolve<IServiceResolver>(), interceptor);
                interceptor = new LogInterceptor(provider.Resolve<IEnumerable<ILogger>>(), provider.Resolve<IAppContext>(), interceptor);
                interceptor = new ValidationInterceptor(provider.Resolve<IServiceResolver>(), interceptor);               
                return interceptor;
            });
            collection.AddSingleton<IMessageBroker, InMemoryBroker>();
        }
    }
}
