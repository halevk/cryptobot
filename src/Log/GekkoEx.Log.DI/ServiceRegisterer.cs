﻿using GekkoEx.DI;
using GekkoEx.Log.InMemory;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace GekkoEx.Log.DI
{
    public class ServiceRegisterer : IServiceRegisterer
    {
        public void Register(IServiceCollection collection)
        {
            collection.AddSingleton<ILogger, InMemoryLogger>();
        }
    }
}
