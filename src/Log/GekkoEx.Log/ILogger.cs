﻿using System;
using System.Threading.Tasks;

namespace GekkoEx.Log
{
    public interface ILogger
    {
        string Name { get; }
        Task Log<T>(T payload);
    }
}
