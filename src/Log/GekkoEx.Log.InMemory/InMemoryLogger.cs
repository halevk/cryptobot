﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;


namespace GekkoEx.Log.InMemory
{
    public class InMemoryLogger : ILogger
    {
        public string Name => "inmemory";
        public static List<object> log = new List<object>();
        public async Task Log<T>(T payload)
        {
            await Task.Run(() => log.Add(payload));
        }        
    }
}
