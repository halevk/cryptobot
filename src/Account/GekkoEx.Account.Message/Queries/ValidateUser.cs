﻿using GekkoEx.Messaging;

namespace GekkoEx.Account.Message.Queries
{
    [RouteHandler(Modul = "account", Route = "validateuser")]
    public class ValidateUser : IRequest<ValidateUserResult>, IValidatable
    {
        public string Email { get; set; }
        public string Password { get; set; }
        public ValidationResult Validate()
        {
            var result = new ValidationResult();
            if (string.IsNullOrWhiteSpace(Email))
                result.AddError("Email can not be empty");
            if (string.IsNullOrWhiteSpace(Password))
                result.AddError("Password can not be empty");
            return result;
        }
    }

    public class ValidateUserResult
    {
        public bool Status { get; set; }
        public string Email { get; set; }
        public string Name { get; set; }
        public string UserName { get; set; }
    }
}
