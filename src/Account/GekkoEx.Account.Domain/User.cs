﻿using GekkoEx.Repository;
using System;

namespace GekkoEx.Account.Domain
{
    public class User : EntityBase
    {
        public string UserName { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Password { get; set; }
        public string[] Groups { get; set; }
        public bool Disabled { get; private set; }
        public string Email { get; set; }
        public DateTime LastLogin { get; private set; }

        public static User New()
        {
            return new User { Id = Guid.NewGuid().ToString() };
        }

        public void SetLastLogin(DateTime time)
        {
            this.LastLogin = time;
        }

        public void SetAccess(bool isDisabled)
        {
            this.Disabled = isDisabled;
        }
    }
}
