﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GekkoEx.Account.Domain.Extension
{
    public static class StringExtension
    {
        public static string HashWithSha256(this string data)
        {
            using (var sha = System.Security.Cryptography.SHA1.Create())
                return ToBase64String(sha.ComputeHash(Encoding.UTF8.GetBytes(data)));
        }

        private static string ToBase64String(byte[] data)
        {
            return Convert.ToBase64String(data);
        }
    }
}
