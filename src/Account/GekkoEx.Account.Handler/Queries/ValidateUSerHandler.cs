﻿using GekkoEx.Account.Domain;
using GekkoEx.Account.Domain.Extension;
using GekkoEx.Account.Message.Queries;
using GekkoEx.Messaging;
using GekkoEx.Repository;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace GekkoEx.Account.Handler.Queries
{
    public class ValidateUserHandler : IRequestHandler<ValidateUser, ValidateUserResult>
    {
        IStorage<User> userRepository;
        public ValidateUserHandler(IStorageFactory storageFactory)
        {
            this.userRepository = storageFactory.New<User>(StorageTypes.Content);
        }
        public async Task<ValidateUserResult> Handle(ValidateUser request, CancellationToken token = default(CancellationToken))
        {
            var result = new ValidateUserResult();
            var user = (await userRepository.FindAsync(p => p.Email == request.Email && p.Password == request.Password.HashWithSha256(), token)).FirstOrDefault();
            if (user != null)
            {
                result.Email = user.Email;
                result.Name = user.Name;
                result.UserName = user.UserName;
                result.Status = true;
            }
            return result;
        }
    }
}
